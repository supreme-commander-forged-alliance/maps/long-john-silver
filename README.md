## Adaptive Long John Silver

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/example-1.png)

_A long forgotten salt lake that was once facilitating an experimentation facility of the Aeon faction_

![](/images/example-2.png)

![](/images/example-3.png)

## Statistics of the map

![](/images/impression03.png)

The map is designed to be either played as a 1 vs 1 or a 2 vs 2. Both teams start on the main land with the intention of one team member moving for the island. The mass doesn't contain any high-concentrated reclaim locations. The expansions should be clear from the design of the map.

There is reclaim on this map. This includes:
 - A few rocks that are used as scenery.
 - A few units (t1 tanks / t1 interceptors).

![](/images/example-4.png)

![](/images/example-5.png)

The map has various pieces of code to add additional feeling to the map. This includes:
 - All initial wreckages can be on fire.
 - The generation of clouds.
 - The map is adaptive - using the adaptive script from the community member CookieNoob.

The map has over 2700 decals in order to make it pretty. That is a lot of them.

## Overview
![](/images/overview.png)

## Tournament
This map was part of a tournament: The SCTA Mapping Tournament. It can be found here:
 - https://forum.faforever.com/topic/153/ta-mapping-tournament-6-10x10-and-under/9

It is based on the original TA map Long Lakes. In the end it doesn't reflect the original map too much - I couldn't find a way to make it look good and work well in Supreme Commander with the constraints the map proposed. Let alone that it is not symmetrical. I also thought the map was a sand-based map due to the water being a salt lake. In retrospect, it is probably a snow-based map.

![](/images/original.png)

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
