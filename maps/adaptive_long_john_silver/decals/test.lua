Decals = {
-- ['5373'] = {
-- type = 'Albedo',
-- name0 = '/env/evergreen/decals/e_bush004_albedo.dds',
-- name1 = '',
-- scale = { 7.06487, 7.06487, 7.06487 },
-- position = { 399.798, 18.2903, 158.706 },
-- euler = { 0, 3.56046, 0 },
-- far_cutoff = 106.066,
-- near_cutoff = 0,
-- remove_tick = 0,
-- },
-- ['5374'] = {
-- type = 'Albedo',
-- name0 = '/env/evergreen/decals/e_bush004_albedo.dds',
-- name1 = '',
-- scale = { 7.06487, 7.06487, 7.06487 },
-- position = { 387.637, 18.2734, 157.719 },
-- euler = { 0, 4.52911, 0 },
-- far_cutoff = 106.066,
-- near_cutoff = 0,
-- remove_tick = 0,
-- },
-- ['5375'] = {
-- type = 'Albedo',
-- name0 = '/env/evergreen/decals/e_bush004_albedo.dds',
-- name1 = '',
-- scale = { 7.06487, 7.06487, 7.06487 },
-- position = { 378.761, 17.3561, 151.132 },
-- euler = { 0, 6.10863, 0 },
-- far_cutoff = 106.066,
-- near_cutoff = 0,
-- remove_tick = 0,
-- },
-- ['5376'] = {
-- type = 'Albedo',
-- name0 = '/env/evergreen/decals/e_bush004_albedo.dds',
-- name1 = '',
-- scale = { 7.06487, 7.06487, 7.06487 },
-- position = { 374.784, 17.2031, 149.567 },
-- euler = { 0, 7.24309, 0 },
-- far_cutoff = 106.066,
-- near_cutoff = 0,
-- remove_tick = 0,
-- },
['5381'] = {
type = 'Albedo',
name0 = '/env/evergreen/decals/e_tarmac04_albedo.dds',
name1 = '',
scale = { 5, 5, 5 },
position = { 383.951, 18.0002, 163.985 },
euler = { 0, 1.57079, 0 },
far_cutoff = 106.066,
near_cutoff = 0,
remove_tick = 0,
},
['5382'] = {
type = 'Albedo',
name0 = '/env/evergreen/decals/e_tarmac04_albedo.dds',
name1 = '',
scale = { 5, 5, 5 },
position = { 374.025, 17.2031, 163.865 },
euler = { 0, 0, 0 },
far_cutoff = 106.066,
near_cutoff = 0,
remove_tick = 0,
},
['5383'] = {
type = 'Albedo',
name0 = '/env/evergreen/decals/e_tarmac04_albedo.dds',
name1 = '',
scale = { 5, 5, 5 },
position = { 388.984, 18.248, 169.022 },
euler = { 0, 3.12413, 0 },
far_cutoff = 106.066,
near_cutoff = 0,
remove_tick = 0,
},
['5384'] = {
type = 'Albedo',
name0 = '/env/evergreen/decals/e_tarmac04_albedo.dds',
name1 = '',
scale = { 5, 5, 5 },
position = { 389.087, 18.1935, 169.029 },
euler = { 0, 4.67747, 0 },
far_cutoff = 106.066,
near_cutoff = 0,
remove_tick = 0,
},
}
