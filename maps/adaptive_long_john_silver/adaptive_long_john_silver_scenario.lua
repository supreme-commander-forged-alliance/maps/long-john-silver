version = 3
ScenarioInfo = {
    name = 'Adaptive Long John Silver',
    description = 'A long forgotton salt lake with ruins from the past. Submission for the SCTA mapping contest. Map made by Jip.',
    type = 'skirmish',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/adaptive_long_john_silver/adaptive_long_john_silver.scmap',
    save = '/maps/adaptive_long_john_silver/adaptive_long_john_silver_save.lua',
    script = '/maps/adaptive_long_john_silver/adaptive_long_john_silver_script.lua',
    norushradius = 0.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    norushoffsetX_ARMY_3 = 0.000000,
    norushoffsetY_ARMY_3 = 0.000000,
    norushoffsetX_ARMY_4 = 0.000000,
    norushoffsetY_ARMY_4 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2','ARMY_3','ARMY_4',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN Remainders' ),
            },
        },
    }}
