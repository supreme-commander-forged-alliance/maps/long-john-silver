
local Utilities = import('/lua/utilities.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua');
local EffectTemplates = import('/lua/EffectTemplates.lua');

local Decals = import('/maps/long lakes/functionality/Decals.lua')

local explosion = import('/lua/defaultexplosions.lua')
local EffectUtilities = import('/lua/EffectUtilities.lua');
local EffectTemplates = import('/lua/EffectTemplates.lua');

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local FireState = {
    RETURN_FIRE = 0,
    HOLD_FIRE = 1,
    GROUND_FIRE = 2,
}

Event = { }
Event.__index = Event

--- Initialise the event
function Event:Setup(
    colossus, 
    chainer,
    shields,
    power,
    others,

    bone,
    start,
    stop,

    decals,
    trail,
    area
)

    -- linking
    event = { }
    setmetatable(event, Event)

    -- store all the units
    event.colossus = colossus
    event.chainer = chainer
    event.shields = shields
    event.power = power
    event.others = others

    -- store the colossi bone rotation
    event.bone = bone
    event.start = start
    event.stop = stop

    -- store the decals
    local lodCutoff = 1000
    local duration = 0
    event.decals = Decals.PlaceFromGPGEditor(decals, lodCutoff, duration)
    event.originals = decals 
    event.trail = trail
    event.area = area

    return event
end

--- Filters the unit by height, such that only units in the valley can be looked at
-- or trigger the colossus in any form of shape
local function FilterUnitsByHeight(units, height, range)

    -- keep all the units that are at this height
    local valids = { }
    for k, unit in units do 
        if not unit.Dead then 
            local y = unit:GetPosition()[2]
            if math.abs(y - height) < range then 
                table.insert(valids, unit)
            end 
        end
    end

    return valids
end

local function ChainColossusThread(event, chainer, colossus)

    -- the structure chaining the colossus has about 12 bones - link them 
    -- to a random bone of the Colossus 
    local connections = { }
    for l = 1, 12 do 
        local ibc = math.floor(Random() * colossus:GetBoneCount());
        connections[l] = ibc
    end

    -- as long as the power generator is alive...
    while event.active do 

        WaitSeconds(0.1)

        -- change up the reclaim beams
        for l = 2, 12 do 
            if Random() < 0.1 then 
                local ibc = math.floor(Random() * colossus:GetBoneCount());
                connections[l] = ibc;
            end
        end

        -- construct the emitters
        for k, connection in connections do 

            -- we need to do this, otherwise they all refer to bone 1
            local icc = k
            if not (connection == -1) then 
                -- beam
                for k, v in EffectTemplates.AeonBuildBeams01 do
                    local fxBeam = CreateBeamEntityToEntity(chainer, icc, colossus, connection, chainer:GetArmy(), v )
                    chainer.Trash:Add(fxBeam)
                end

                -- hit
                for k, v in EffectTemplates.AHighIntensityLaserHitUnit01 do
                    if Random() < 0.2 then 
                        local hit = CreateAttachedEmitter(colossus, connection, "Remainders", v)
                        chainer.Trash:Add(hit)
                    end
                end
            end
        end
    end
end

-- local bone = 'Left_Arm_B01'
-- local rotator = CreateRotator(colossus, bone, 'z',  60, 10, 1, 10)
-- local rotator = CreateRotator(colossus, bone, 'x',  -40, -10, -1, -10)

-- Puts the colossus on its knees and makes it wobble a bit
local function AnimateColossusThread(event, colossus)

    -- retrieve the death animation
    local bp = colossus:GetBlueprint();
    local anims = bp.Display;
    local animDeathCount = table.getn(anims.AnimationDeath)
    local animDeathIndex = Utilities.GetRandomInt(1, animDeathCount)
    local animDeath = anims.AnimationDeath[animDeathIndex]

    -- initialise the stance
    event.animator = CreateAnimator(colossus)
    event.animator:PlayAnim(animDeath.Animation)
    event.animator:SetAnimationTime(2.5)

    -- make it wobble
    t = 0
    while event.active do 

        -- wait up a bit
        WaitSeconds(0.1)

        -- compute the new rate and change it up
        t = t + 0.1
        local rate = 0.5 * math.sin(t) + 0.1 * math.cos(t / 2)
        event.animator:SetRate(rate)
    end
end

--- Makes the colossus point towards 
local function ColossusHelpThread(event, colossus)

    local radius = 20
    local position = colossus:GetPosition()

    while event.active do 

        WaitSeconds(2.0)

        -- assume we don't point to the power generator

        -- get all the units in some radia
        local units = Utilities.GetTrueEnemyUnitsInSphere(
            colossus, position, radius, categories.LAND - categories.STRUCTURE
        )
        
        -- keep the ones in the valley
        units = FilterUnitsByHeight(
            units, position[2], 2
        )

        if units then 

            -- point our eye at them
            local count = table.getn(units)
            if count > 0 then 
                local index = 1 + math.floor(count * Random())
                local weapon = colossus:GetWeapon(1)
                weapon:SetTargetEntity(units[index])
            end

            -- see if there is a commander
            local commanders = EntityCategoryFilterDown(categories.COMMAND, units)
            if table.getn(commanders) > 0 then 
                event.pointToPowerGen = true
            end
        end
    end
end

function MakeImmune(unit)
    unit:SetReclaimable(false);
    unit:SetCapturable(false);
    unit:SetDoNotTarget(true);
    unit:SetRegenRate(5000);
    unit:SetUnSelectable(true);
    unit:SetCanTakeDamage(false);
end

function MakeAllImmune(units)
    for k, unit in units do 
        MakeImmune(unit)
    end
end

function DeathThread (self)
    self:PlayUnitSound('Destroyed')
    explosion.CreateDefaultHitExplosionAtBone(self, 'Torso', 4.0)
    explosion.CreateDebrisProjectiles(self, explosion.GetAverageBoundingXYZRadius(self), {self:GetUnitSizes()})
    WaitSeconds(2)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B02', 1.0)
    WaitSeconds(0.1)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B01', 1.0)
    WaitSeconds(0.1)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Left_Arm_B02', 1.0)
    WaitSeconds(0.3)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Arm_B01', 1.0)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B01', 1.0)

    WaitSeconds(3.5)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Torso', 5.0)

    if self.DeathAnimManip then
        WaitFor(self.DeathAnimManip)
    end

    local bp = self:GetBlueprint()
    local position = self:GetPosition()
    local qx, qy, qz, qw = unpack(self:GetOrientation())
    local a = math.atan2(2.0 * (qx * qz + qw * qy), qw * qw + qx * qx - qz * qz - qy * qy)
    for i, numWeapons in bp.Weapon do
        if bp.Weapon[i].Label == 'CollossusDeath' then
            position[3] = position[3]+5*math.cos(a)
            position[1] = position[1]+5*math.sin(a)
            -- DamageArea(self, position, bp.Weapon[i].DamageRadius, bp.Weapon[i].Damage, bp.Weapon[i].DamageType, bp.Weapon[i].DamageFriendly)
            break
        end
    end

    self:PlayUnitSound('Destroyed')
    explosion.CreateDefaultHitExplosionAtBone(self, 'Torso', 2.0)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B02', 1.0)
    WaitSeconds(0.1)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B01', 1.0)
    WaitSeconds(0.1)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Left_Arm_B02', 1.0)
    WaitSeconds(0.1)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Arm_B01', 1.0)
    explosion.CreateDefaultHitExplosionAtBone(self, 'Right_Leg_B01', 1.0)

    self:DestroyAllDamageEffects()

    self:Destroy()
end

-- Launches the event
function Event:Launch()

    self.active = true

    -- make everything immune
    MakeImmune(self.colossus)
    MakeImmune(self.chainer)
    MakeAllImmune(self.shields)
    MakeAllImmune(self.others)

    -- prepare the colossus
    self.colossus:SetFireState(FireState.HOLD_FIRE)
    self.rotator = CreateRotator(self.colossus, self.bone, 'z',  0, 0, 0, 0)

    -- make it animate
    ForkThread(AnimateColossusThread, self, self.colossus)

    -- make it chained
    ForkThread(ChainColossusThread, self, self.chainer, self.colossus)

    -- make it appear to ask for help
    ForkThread(ColossusHelpThread, self, self.colossus)

    -- make it all end when the power dies
    ScenarioFramework.CreateUnitDeathTrigger(
        function()

            -- create explosions at the back
            self.active = false

            -- kill everything
            explosion.CreateDefaultHitExplosionAtBone(self.chainer, 1, 4.0)
            self.chainer:Kill()

            --self.colossus:Kill()

            -- destroy the collossus
            self.colossus.Dead = true
            self.colossus:DisableUnitIntel('Killed')
            self.colossus:ForkThread(DeathThread, self.colossus)
            self.colossus:ForkThread(self.colossus.PlayAnimationThread, 'AnimationDeath')
            self.DisallowCollisions = true

            -- destroy the shields
            ForkThread(
                function() 
                    for k, shield in self.shields do 
                        WaitSeconds(0.2 * Random())
                        explosion.CreateDefaultHitExplosion(shield, 2.0)
                        shield:Kill()
                    end
                end
            )

            -- destroy all the other buildings
            ForkThread(
                function() 
                    for k, other in self.others do 
                        WaitSeconds(0.2 * Random())
                        explosion.CreateDefaultHitExplosion(other, 2.0)
                        other:Kill()
                    end
                end
            )

            -- replace the decals
            for k, decal in self.decals do 
                decal:Destroy()
            end

            local lodCutoff = 1000
            local duration = 40
            self.decals = Decals.PlaceFromGPGEditorRange(self.originals, lodCutoff, duration, 10)
            self.trails = Decals.PlaceFromGPGEditor(self.trail, 1000, 0)
        
        end,
        self.power
    )

end
