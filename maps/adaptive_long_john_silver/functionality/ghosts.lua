
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua');
local EffectTemplates = import('/lua/EffectTemplates.lua');

local util = import('/lua/utilities.lua')

local decalsTest = import('/maps/long lakes/decals/test.lua').Decals
local decalsTop = import('/maps/long lakes/decals/colossus-top.lua').Decals
local decalsBot = import('/maps/long lakes/decals/colossus-bot.lua').Decals

local Decals = import('/maps/long lakes/functionality/Decals.lua')

local units = nil

animData = {
    {
        rate = -1,
        time = 4,
        wait = 1.5,
    },
    {
        rate = -1,
        time = 4,
        wait = 1.5,
    },

    {
        rate = 1,
        time = 4,
        wait = 0.6,
    },

    {
        rate = -1,
        time = 4,
        wait = 1.5,
    },

    {
        rate = 0.25,
        time = 4,
        wait = 1.5,
    },

    {
        rate = 0,
        time = 3,
        wait = 1.5,
    },
}

local fire = 10;

--- Filters the unit by height, such that only units in the valley can be looked at
-- or trigger the colossus in any form of shape
function FilterUnitsByHeight(colossus, units)

    -- determine the base range R = [b - r, b + r]
    local range = 2.0
    local base = colossus:GetPosition()[2]

    -- keep all the units that are at this height
    local valids = { }
    for k, unit in units do 
        if not unit.Dead then 
            local height = unit:GetPosition()[2]
            if math.abs(height - base) < range then 
                table.insert(valids, unit)
            end 
        end
    end

    return valids
end

--- Filters the unit by type, such that only certain units will trigger the colossus
function FilterUnitsByCategory(colossus, units)
    return EntityCategoryFilterDown((categories.LAND - categories.STRUCTURE), units)
end

--- The colossus looks at a random units
function ColossusLookAtUnit(colossus, units)

    if units then 
        local count = table.getn(units)

        if count > 0 then 
            -- choose a random unit
            local index = 1 + math.floor(count * Random())
            local unit = units[index]

            -- make the bulb look at it
            local weapon = colossus:GetWeapon(1);
        end

    end
    
end

-- function CreateDecal(position, heading, textureName1, textureName2, type, sizeX, sizeZ, lodParam, duration, army, fidelity)
-- end

--- Constructs a bunch of decals that remain. 
function ConstructDecalsInfinite(data)
    local decals = { }
    for k, decal in data do 

        local orientation = decal.euler[2]
        local offset = Vector(decal.scale[1] * 0.5, 0, decal.scale[3] * 0.5)
        local rotated = Vector(
            math.cos(orientation) * offset[1] - math.sin(orientation) * offset[3],
            0,
            math.sin(orientation) * offset[1] + math.cos(orientation) * offset[3]
        )

        local position = Vector(decal.position[1] + rotated[1], 0, decal.position[3] + rotated[3])

        LOG("decal position: " .. repr(decal.position))
        LOG("computed posit: " .. repr(position))
        LOG("offset        : " .. repr(offset))
        LOG("rotated offset: " .. repr(rotated))
        LOG("orientation   : " .. repr(orientation))

        LOG(repr(decal))
        table.insert(
            decals, 
            CreateDecal(
                position,
                orientation,
                decal.name0, 
                decal.name1, 
                decal.type, 
                decal.scale[1], 
                decal.scale[3], 
                1000, 
                0, 
                1,
                0
            )
        )
    end
    return decals
end

--- Constructs a bunch of decals that dissapear after 20 - 40 seconds.
function ConstructDecalsFinite(data)
    local decals = { }
    for k, decal in data do 
        local duration = 20 + math.floor(Random() * 20)
        LOG(repr(decal))
        table.insert(
            decals, 
            CreateDecal(
                decal.position, 
                decal.euler[2], 
                decal.name0, 
                decal.name1, 
                decal.type, 
                decal.scale[1], 
                decal.scale[3], 
                1000, 
                duration, 
                armyIndex
            )
        )
    end
    return decals
end

--- The colossus starts pointing at the center
function ColossusStartPointing(rotator, data, colossus)

end

--- The colossus stops pointing at the center
function ColossusStopPointing(rotator, data, colossus)

end

function Tick()



    Decals.PlaceFromGPGEditor(decalsTop, 1000, 0)

    local colossus = ScenarioUtils.CreateArmyGroup("Remainders", "Chainee", false)[1]
    local chainer = ScenarioUtils.CreateArmyGroup("Remainders", "Chainer", false)[1]
    local other = ScenarioUtils.CreateArmyGroup("Remainders", "Others", false)[1]
    local power = ScenarioUtils.CreateArmyGroup("Remainders", "Power", false)[1]



    LOG("Tick!")

    -- aeon_heavydisruptor_cannon_projectile_hit_01_emit.bp
    -- aeon_missiled_hit_01_emit.bp
    
    connections = { }

    for l = 1, 12 do 
        local ibc = math.floor(Random() * colossus:GetBoneCount());
        connections[l] = ibc
    end

    LOG(repr(connections))

    ForkThread(
        function()
            while true do 
                WaitSeconds(0.1)

                -- change them up every now and then
                for l = 2, 12 do 
                    if Random() < 0.1 then 
                        local ibc = math.floor(Random() * colossus:GetBoneCount());
                        connections[l] = ibc;
                    end
                end

                -- make the beams
                for k, connection in connections do 

                    local icc = k
                    if not (connection == -1) then 
                        -- beam
                        for k, v in EffectTemplates.AeonBuildBeams01 do
                            local fxBeam = CreateBeamEntityToEntity(chainer, icc, colossus, connection, chainer:GetArmy(), v )
                            chainer.Trash:Add(fxBeam)
                        end

                        -- hit
                        for k, v in EffectTemplates.AHighIntensityLaserHitUnit01 do
                            if Random() < 0.2 then 
                                local hit = CreateAttachedEmitter(colossus, connection, "Remainders", v)
                                chainer.Trash:Add(hit)
                            end
                        end
                    end
                end
            end
        end
    )

    


    ForkThread(
        function()
            WaitSeconds(5.0)
                -- put it all on fire
                for k = 1, fire do 
                    local effects = EffectTemplates.TreeBurning01;
                    local indexBone = math.floor(Random() * colossus:GetBoneCount());
                    local bone = colossus:GetBoneName(indexBone);
                    for k, effect in effects do 
                        local emitter = CreateAttachedEmitter(colossus, bone, "Remainders", effect)
                        colossus.Trash:Add(emitter)
                    end
                end

                local bp = colossus:GetBlueprint();
                local anims = bp.Display;
                
                local animDeathCount = table.getn(anims.AnimationDeath)
                local animDeathIndex = util.GetRandomInt(1, animDeathCount)
                local animDeath = anims.AnimationDeath[animDeathIndex]

                local animator = CreateAnimator(colossus)
                animator:PlayAnim(animDeath.Animation)

                local weapon = colossus:GetWeapon(2)
                weapon:SetFiringRandomness(1000)
                weapon:SetTargetGround(ScenarioUtils.GetMarker('Target').position)

                local bone = 'Left_Arm_B01'
                local rotator = CreateRotator(colossus, bone, 'z',  60, 10, 1, 10)
                local rotator = CreateRotator(colossus, bone, 'x',  -40, -10, -1, -10)

                animator:SetAnimationTime(2.5)
                ForkThread(
                    function()
                        t = 0
                        while true do 
                            WaitSeconds(0.1)
                            t = t + 0.1
                            local rate = 0.5 * math.sin(t) + 0.1 * math.cos(t / 2)
                            animator:SetRate(rate)
                        end
                    end
                )
        end
    )
end

Tick();
