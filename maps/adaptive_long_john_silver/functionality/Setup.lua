
local Weather = import('/maps/adaptive_long_john_silver/functionality/Weather.lua')

function OnStart()

    -- do all the weather 
    Weather.CreateWeather()

    -- make all the wrecks
    doscript('/maps/adaptive_long_john_silver/functionality/Wreckages.lua')

end
