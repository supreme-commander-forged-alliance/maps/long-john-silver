local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Colossi = import('/maps/long lakes/events/Colossi.lua').Event
local Weather = import('/maps/long lakes/functionality/Weather.lua')

function OnPopulate()
	
	ScenarioUtils.InitializeArmies()
	ScenarioFramework.SetPlayableArea(ScenarioUtils.AreaToRect('playable-area'))
end

function OnStart(self)

	-- spawn 'dem clouds
	Weather.CreateWeather()

	-- -- shared information between the colossi
	-- local power = ScenarioUtils.CreateArmyGroup("Remainders", "power", false)[1]
	-- local others = ScenarioUtils.CreateArmyGroup("Remainders", "others-mid", false)[1]

	-- -- for the colossus on the bottom
    -- local chainerBot = ScenarioUtils.CreateArmyGroup("Remainders", "chainer-bot", false)[1]
	-- local chaineeBot = ScenarioUtils.CreateArmyGroup("Remainders", "chainee-bot", false)[1]
	-- local shieldsBot = ScenarioUtils.CreateArmyGroup("Remainders", "shields-bot", false)
	-- local othersBot = ScenarioUtils.CreateArmyGroup("Remainders", "others-bot", false)
	-- local decalsBot = import("/maps/long lakes/decals/colossus-bot.lua").Decals
	-- local trailBot = import("/maps/long lakes/decals/death-trail-bot.lua").Decals
	-- local areaBot = ScenarioUtils.AreaToRect('colossus-bot')

	-- local eventBot = Colossi:Setup(
	-- 	chaineeBot,
	-- 	chainerBot,

	-- 	shieldsBot,
	-- 	power,
	-- 	othersBot,

	-- 	'Right_Arm_B01',
	-- 	{ },
	-- 	{ },

	-- 	decalsBot,
	-- 	trailBot,
	-- 	areaBot
	-- )

	-- eventBot:Launch()

	-- -- for the colossus on the top
    -- local chainerTop = ScenarioUtils.CreateArmyGroup("Remainders", "chainer-top", false)[1]
	-- local chaineeTop = ScenarioUtils.CreateArmyGroup("Remainders", "chainee-top", false)[1]
	-- local shieldsTop = ScenarioUtils.CreateArmyGroup("Remainders", "shields-top", false)
	-- local othersTop = ScenarioUtils.CreateArmyGroup("Remainders", "others-top", false)
	-- local decalsTop = import("/maps/long lakes/decals/colossus-top.lua").Decals
	-- local trailTop = import("/maps/long lakes/decals/death-trail-top.lua").Decals
	-- local areaTop = ScenarioUtils.AreaToRect('colossus-top')

	-- local eventTop = Colossi:Setup(
	-- 	chaineeTop,
	-- 	chainerTop,

	-- 	shieldsTop,
	-- 	power,
	-- 	othersTop,

	-- 	'Left_Arm_B01',
	-- 	{ },
	-- 	{ },

	-- 	decalsTop,
	-- 	trailTop,
	-- 	areaTop
	-- )

	-- eventTop:Launch()

end
