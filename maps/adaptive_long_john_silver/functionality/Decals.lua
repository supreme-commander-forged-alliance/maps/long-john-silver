
--- Places decals that are output of the export function of the GPG editor. These decals
-- need special attention in order to place them correctly. 
-- @param data The data from the GPG editor
-- @param cutoff The cutoff of the decals. Optional, overrides the value defined in the decal if set.
-- @param duration The duration of the decals, Optional, overrides the value defined in the decal if set. A value of 0 means infinite.
function PlaceFromGPGEditor(data, cutoff, duration)

    -- pivot point as CreateDecal expects it, versus 
    -- the pivot point exported by the GPG editor

    --  x-----x     o-----x  
    --  |     |     |     |
    --  |  o  |     |     |
    --  |     |     |     |
    --  x-----x     x-----x

    -- Depending on the orientation, the pivot point can
    -- be at any point of the unit circle. This function 
    -- takes this offset into account

    local decals = { }
    for k, decal in data do 

        -- pre-compute the cos and sin values
        local orientation = decal.euler[2]
        local c = math.cos(orientation)
        local s = math.sin(orientation)

        -- perform a matrix multiplication with a 2 dimensional rotation matrix
        local offset = Vector(decal.scale[1] * 0.5, 0, decal.scale[3] * 0.5)
        local rotated = Vector(
            c * offset[1] - s * offset[3],
            0,
            s * offset[1] + c * offset[3]
        )

        -- compute the real position
        local position = Vector(decal.position[1] + rotated[1], 0, decal.position[3] + rotated[3])

        -- construct them accordingly
        table.insert(
            decals, 
            CreateDecal(
                position,                               -- position
                orientation,                            -- orientation on the y axis
                decal.name0,                            -- first texture
                decal.name1,                            -- second texture
                decal.type,                             -- type: 'Albedo', 'Normal', 'Water Mask', 'Water Albedo', 'Water Normals', 'Glow', 'Alpha Normals' or 'Glow Mask'
                decal.scale[1],                         -- scale, generally uniform with the next parameter
                decal.scale[3],                         -- scale, generally uniform with the previous parameter
                cutoff or decal.far_cutoff,             -- LOD cut-off: when it fades out
                duration or decal.remove_tick,          -- duration
                1,                                      -- army, irrelevant parameter
                0                                       -- fidelity?
            )
        )
    end

    -- return them all, in case we want to do something with them :)
    return decals
end

--- Places decals that are output of the export function of the GPG editor. These decals
-- need special attention in order to place them correctly. All decals have a duration 
-- that fits inside a certain range - making them dissapear one by one.
-- @param data The data from the GPG editor
-- @param cutoff The cutoff of the decals. Optional, overrides the value defined in the decal if set.
-- @param duration The duration of the decals, Optional, overrides the value defined in the decal if set. A value of 0 means infinite.
-- @param range The range of the duration, e.g, a value is chosen betwee [duration - range, duration + range].
function PlaceFromGPGEditorRange(data, cutoff, duration, range)

    for k, decal in data do 
        decal.remove_tick = duration - 2 * math.floor(range * Random())
    end

    return PlaceFromGPGEditor(data, cutoff)
end