
do 
    -- find all the players
    local armies = ListArmies();
    local players = { };

    for v, army in armies do
        if string.find(army, "ARMY") then
            table.insert(players, army);
        end
    end

    -- make them neutral to the remainders
    for k, player in players do 
        SetAlliance(player, "Remainders", "Enemy");
    end
end